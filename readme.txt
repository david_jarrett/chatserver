This is a server for a basic chat application.

Protocols

Connection:
When you connect to the server, it will respond to you with the message "REQUEST_USERNAME".
Your next message to the server will be a string containing the userName.

If the operation is successful, you will receive the message "OK".

If a user by that name is already chatting, you will get "USER_EXISTS", and the server
will rebroadcast the REQUEST_USERNAME message and be standing by for an alternate username.

Disconnection:
When you are ready to disconnect, send the server the message "TERMINATE".
You will receive the response "TERMINATED".

User list:
If you send the message USER_LIST to the server it will will respond with a String
containing all the logged in user's names.

--Addendum: When a new user logs on or off of the server, the server will send out the mass
message "UPDATE_USERLISTS".