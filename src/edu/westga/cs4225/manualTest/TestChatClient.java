package edu.westga.cs4225.manualTest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class TestChatClient extends Thread {

	private Socket clientSocket;
	private InputStreamReader incomingMessages;
	private InputStreamReader keyboardInput;
	private PrintStream outgoingMessages;
	private BufferedReader reader;
	
	private boolean isInitialized = false;
	
	public TestChatClient(Socket socket) {
		this.clientSocket = socket;
		
		try {
			this.outgoingMessages = new PrintStream(clientSocket.getOutputStream());
			this.incomingMessages = new InputStreamReader(clientSocket.getInputStream());
			this.keyboardInput = new InputStreamReader(System.in);
			this.reader = new BufferedReader(this.incomingMessages);
			
			System.out.println(this.reader.readLine());
			String userName = new BufferedReader(this.keyboardInput).readLine();
			this.outgoingMessages.println(userName);
			
			this.isInitialized = true;
		
		} catch (UnknownHostException e) {
			System.err.println("Problem with the host.");
		} catch (IOException e) {
			System.err.println("Unable to connect.");
		}
	}
	
	@Override
	public void run() {
				
	}
	
	public String getMessage() throws IOException {
		return this.reader.readLine();
	}
	
	public void sendMessage(String message) {
		this.outgoingMessages.println(message);
	}
	
	public boolean getIsInitialized() {
		return this.isInitialized;
	}
}
