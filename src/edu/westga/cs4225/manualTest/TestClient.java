package edu.westga.cs4225.manualTest;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 * Represents a client in a Client/Server setup.
 * 
 * @author CS 4225
 * @version Spring 2019
 *
 */
public class TestClient {

	private static final String HOST = "localhost";
	private static final int PORT = 4225; // #convention for CS 4225

	/**
	 * Main entry point of client.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		try {
			System.err.println("Session started... [Type 'END' to end session]");
			String message;

			var socket = new Socket("localhost", 4225);
			var client = new TestChatClient(socket);
			// client.start();

			Runnable network = () -> {
				String msg = "";
				try {
					while (!msg.equals("TERMINATE")) {
						msg = client.getMessage();
						System.out.println(msg);
					}
				} catch (IOException e) {
					System.out.println("Bye!");
				}
			};

			var networkThread = new Thread(network);
			networkThread.start();

			Runnable keyboard = () -> {
				String msg;
				while (true) {
					msg = scan.nextLine();
					client.sendMessage(msg);
				}
			};

			while (!client.getIsInitialized()) {
			}

			var keyboardThread = new Thread(keyboard);
			keyboardThread.start();
			networkThread.join();

		} catch (UnknownHostException e) {
			System.err.println("Trying to connect to unknown host: " + e);
		} catch (IOException e) {
			System.err.println("IOException:  " + e);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}