package edu.westga.cs4225.model;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Manages all connections for the server in separate threads.
 * 
 * @author David Jarrett
 * @version 03/22/2019
 */
public class ConnectionManager {

	private static final String SERVER_CHAT_HANDLE = "Server";
	private static final String USER_TAG = "USER:";
	
	Map<String, ClientHandler> activeUsers = new ConcurrentHashMap<>();

	/**
	 * Determines if the given userName is in use on the server.
	 * @preconditions - userName != null
	 * @param userName - the userName to check.
	 * @return true if the userName is in use, false otherwise
	 */
	public boolean isActiveUser(String userName) {
		Objects.requireNonNull(userName, "userName cannot be null");
		return this.activeUsers.containsKey(userName);
	}

	/**
	 * Adds a new client to the server's management pool.
	 * @preconditions - userName != null && client != null
	 * @postconditions - client will now be managed by the connection manager
	 * 					 in its own thread.
	 * @param userName - userName of client
	 * @param client - client object which manages the client connection
	 */
	public void addActiveUser(String userName, Client client) {
		var handler = new ClientHandler(client);
		this.activeUsers.put(userName, handler);
		handler.start();
	}
	
	/**
	 * Broadcasts a server-wide message. Use for messages from the server only.
	 * @preconditions - none
	 * @param message - message to be broadcasted.
	 */
	public void broadcast(String message) {
		this.broadcast(SERVER_CHAT_HANDLE, message);
	}
	
	/**
	 * Broadcasts a server-wide message. Use for messages from anyone.
	 * @preconditions - userName != null
	 * @param message - message to be broadcasted.
	 */
	public void broadcast(String userName, String message) {
		Objects.requireNonNull(userName, "userName cannot be null");
		for (var client : this.activeUsers.values()) {
			client.sendMessage(userName + ": " + message);
			client.logMessage(userName, message);
		}
	}
	
	/**
	 * Returns a list of active users (userNames)
	 * @preconditions - none
	 * @return List of userNames, terminated with "USERS_END"
	 */
	public String getUserList() {
		StringBuilder sb = new StringBuilder();
		this.activeUsers.values().forEach(client -> sb.append(USER_TAG + client.getUserName() + '\n'));
		sb.append("USERS_END");
		return sb.toString();
	}
	
	/* Internal class used by the ConnectionManager to wrap client objects in a
	 * thread so that they can interact continuously. Should not be publicly documented.
	 * 
	 * @author David Jarrett
	 * @version 03/22/2019
	 */
	private class ClientHandler extends Thread {
		
		private static final String UPDATE_USERLISTS_MSG = "UPDATE_USERLISTS";
		private Client client;
		private StringBuilder messageHistory;
		
		public ClientHandler(Client client) {
			this.client = client;
			this.messageHistory = new StringBuilder();
		}
		
		public void sendMessage(String message) {
			this.client.sendMessage(message);
		}
		
		/*
		 * Listens for and responds to messages from the client until receiving the
		 * TERMINATE signal.
		 * @see java.lang.Thread#run()
		 */
		@Override
		public void run() {
			String message;
			try {
				while (!(message = this.client.getMessage()).equals("TERMINATE")) {
					this.handleMessage(this.client.getUserName(), message);
				}
				this.terminateSession();
			} catch (IOException e) {
				System.err.println(e.getMessage());
			}
		}

		private void terminateSession() {
			activeUsers.remove(this.client.getUserName());
			this.client.sendMessage("TERMINATED");
			broadcast(this.client.getUserName() + " has left the conversation :(");
			broadcast(UPDATE_USERLISTS_MSG);
			
			String fileName = this.client.getUserName() + "_" + LocalDateTime.now() + ".txt";
			fileName = fileName.replace(':', '.');
			try (var outFile = new PrintWriter(new File(fileName))){
				outFile.print(this.messageHistory.toString());
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			this.client.close();
		}
		
		private void handleMessage(String userName, String message) {
			switch (message) {
			case "USER_LIST":
				this.client.sendMessage(getUserList());
				break;
			default:
				message = this.filter(message);
				broadcast(userName, message);
			}
		}

		private void logMessage(String userName, String message) {
			var time = LocalDateTime.now();
			this.messageHistory.append(time + ", " + userName + ", " + message + "\n");
		}

		/*
		 * Special feature that filters basic profanity. Sorry for the, uh, profanity.
		 */
		private String filter(String message) {
			String[] patterns = {
				"(?i)fuck",
				"(?i)shit",
				"(?i)damn",
				"\\b((?i)dumb|(?i)stupid)?(?i)ass((?i)hole(s)?|(?i)wipe(s)?)?(es)?\\b",
			};
			
			for (var i = 0; i < patterns.length; i++) {
				message = message.replaceAll(patterns[i], "#!@%");
			}
			return message;
		}
		
		public String getUserName() {
			return this.client.getUserName();
		}
		
	}

}
