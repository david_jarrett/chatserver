package edu.westga.cs4225.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Objects;

public class Client {

	private Socket clientSocket;
	private InputStreamReader incomingMessages;
	private PrintStream outgoingMessages;
	private BufferedReader bufferedReader;
	
	private String userName;

	public Client(Socket clientSocket) throws IOException {
		this.clientSocket = Objects.requireNonNull(clientSocket, "clientSocket must not be null");

		this.incomingMessages = new InputStreamReader(this.clientSocket.getInputStream());
		this.outgoingMessages = new PrintStream(this.clientSocket.getOutputStream());
		this.bufferedReader = new BufferedReader(incomingMessages);
	}

	public void sendMessage(String message) {
		this.outgoingMessages.println(message);
	}

	public String getMessage() throws IOException {
		return this.bufferedReader.readLine();
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserName() {
		return this.userName;
	}
	
	public void close() {
		try {
			this.incomingMessages.close();
			this.outgoingMessages.close();
			this.bufferedReader.close();
			this.clientSocket.close();
		} catch (IOException e) {
			System.out.println(e);
		}
	}
}
