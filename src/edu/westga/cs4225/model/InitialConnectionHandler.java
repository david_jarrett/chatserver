package edu.westga.cs4225.model;

import java.io.IOException;
import java.net.Socket;
import java.util.Objects;

/**
 * This class handles an incoming connection. Once the connection is
 * established and the client object is created, responsibility for the client is handed
 * off to the ConnectionManager.
 * 
 * @author David Jarrett
 * @version 03/22/2019
 */
public class InitialConnectionHandler extends Thread {

	public static final String USER_NAME_PROMPT = "REQUEST_USERNAME";
	public static final String USER_EXISTS_PROMPT = "USER_EXISTS";
	public static final String OK_PROMPT = "OK";

	private Socket clientSocket;
	private ConnectionManager connectionManager;

	/**
	 * Uses an existing socket and ConnectionManager.
	 * @preconditions - client must be an open socket
	 * 				  - connectionManager must not be null
	 * @param client - socket for a client that has just connected to the server
	 * @param connectionManager - will manage the client object created by this handler
	 */
	public InitialConnectionHandler(Socket client, ConnectionManager connectionManager) {
		this.clientSocket = Objects.requireNonNull(client, "client must not be null");
		this.connectionManager = Objects.requireNonNull(connectionManager, "connectionManager must not be null");
	}

	/**
	 * Validates the username given by the client, requiring the username not be currently
	 * in use on the server. Then creates a client object to manage the client connection
	 * and passes it off to the connection manager. Also broadcasts to the universe that
	 * a new user has arrived.
	 */
	@Override
	public void run() {
		try {
			Client client = new Client(this.clientSocket);
			String userName = null;

			while (userName == null) {
				client.sendMessage(USER_NAME_PROMPT);
				userName = client.getMessage();
				if (this.connectionManager.isActiveUser(userName)) {
					client.sendMessage(USER_EXISTS_PROMPT);
					userName = null;
				} else {
					client.setUserName(userName);
					this.connectionManager.addActiveUser(userName, client);
					this.connectionManager.broadcast(userName + " has joined the conversation!");
					this.connectionManager.broadcast("UPDATE_USERLISTS");
					client.sendMessage(OK_PROMPT);
				}
			}
		} catch (IOException e) {
			System.err.println(e);
		}
	}
}
