package edu.westga.cs4225.app;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import edu.westga.cs4225.model.ConnectionManager;
import edu.westga.cs4225.model.InitialConnectionHandler;

/**
 * This is the main server loop. It runs in a single thread, spawning new threads
 * to handle incoming requests.
 * 
 * @author David Jarrett
 * @version 03/22/2019
 */
public class Driver {

	private static final int PORT = 4225;

	/**
	 * Application entry-point
	 * @param args not used
	 */
	public static void main(String[] args) {
		ServerSocket serverSocket = getServerSocket();
		Socket clientSocket = null;
		ConnectionManager connectionManager = new ConnectionManager();

		try {
			while (true) {
				clientSocket = serverSocket.accept();
				new InitialConnectionHandler(clientSocket, connectionManager).start();
				clientSocket = null;
			}
		} catch (IOException e) {
			System.err.println("Error: " + e);
		}
	}

	private static ServerSocket getServerSocket() {
		try {
			return new ServerSocket(PORT);
		} catch (IOException e) {
			System.err.println("Could not open serverSocket on port " + PORT);
			System.exit(-1);
		}
		return null;
	}

}
